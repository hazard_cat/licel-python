import click
from licel import process, calcerror

@click.group()
def cli():
    pass


@cli.command()
@click.option('--combined/--no-combined', default=False, help='Усредняем фходные файлы перед расчетом')
@click.option('--cconst', type=float, default=8.70, help='Калибровочная константа для пересчета в отношение смеси')
@click.option('--decim-idx', type=click.IntRange(0, 5), default=5, help='Параметр пространственного разрешения')
@click.option('--model', type=click.File(), default='model.txt', help='Файл с данными о плотности атмосферы')
@click.option('--meteo', type=click.File(), help='Файл с данными о метеопараметрах для верификации')
@click.option('--display', is_flag=True, default=False, help='Рисовать или нет результат расчета')
@click.argument('files', nargs=-1, type=click.Path(), required=True)
def mixingratio(combined, cconst, decim_idx, model, meteo, files, display):
    if not combined and len(files)>1:
        files = (files[0],)

    process(files, model, meteo, decim_idx, combined=combined, Cconst=cconst, display=display)
    

@cli.command()
@click.argument('files', nargs=-1, type=click.Path(), required=True)
def error(files):
    if len(files)<=1:
        raise Exception("You should provide more than single file")
    
    calcerror(files)


if __name__ == "__main__":
    cli()
    

