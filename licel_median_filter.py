import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler
from licel.licel import LicelFile, save_netcdf
import click

class FileCreateHandler(FileSystemEventHandler):
    """
    """
    def __init__(self, outpath, filter, nc_output, netcdf):
        super(FileCreateHandler, self).__init__()
        self.outpath = outpath
        self.filter = filter
        self.netcdf = netcdf
        self.nc_output = nc_output
        
    def on_created(self, evt):
        """
        """
        f = LicelFile(evt.src_path, filter=self.filter)
        
        f.save(self.outpath)
        
        if self.netcdf:
            save_netcdf([f], self.nc_output)
        
        
        
@click.command()
@click.option('--inpath', default='.', help=u'Путь для отслеживания появления новых файлов')
@click.option('--outpath', default='.', help=u'Путь для сохранения преобразованных файлов')
@click.option('--nc-output', default='.', help=u'Путь для сохранения преобразованных файлов в формате netcdf4')
@click.option('--netcdf', is_flag=True, help=u'Если указан, будет выполнено сохранение данных в формате netcdf4')
@click.option('--filter', is_flag=True, help=u'Если указан, к аналоговым каналам файла данных будет применена медианная фильтрация с окном 3')
def start(inpath, outpath, nc_output, netcdf, filter):    
    event_handler = FileCreateHandler(outpath, filter, nc_output, netcdf)    
    observer = Observer()
    observer.schedule(event_handler, inpath, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print(u"Завершение работы приложения для мониторинга...")
        observer.stop()

    observer.join()
    

if __name__=='__main__':
    start()