from setuptools import setup, find_packages

setup(
    name='licel_median_filter',
    version='1.1',
    packages = ['licel'],
    include_package_data=True,
    py_modules=['licel_median_filter', 'startGUI'],
    install_requires=[
        'click',
        'numpy',
        'h5py'
    ],
    entry_points='''
        [console_scripts]
        licel_median_filter=licel_median_filter:start
        [gui_scripts]
        licel_median_gui=starGUI:start
    ''',
)
