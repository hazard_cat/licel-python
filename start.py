from licel.licel import LicelFile, save_netcdf
import sys


if __name__=="__main__":
    f=LicelFile("a1622010.045801", filter=True)
    print(f.dataset[0].wavelength, f.dataset[0].deviceId)
    print(f.measurementStartTime, f.longitude, f.latitude, f.zenith)
    print("Size of LicelFile object is {!r}".format(sys.getsizeof(f)))
    #for item in f.dataset:
    #    plt.semilogy(item.data[:1000])

    #plt.grid(True)
    f.save("./")
    save_netcdf([f])