#!/Users/kshmirko/.miniconda3/envs/science/bin/python

#-*- encoding: utf-8 -*-
from tkinter import *

import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler
from licel.licel import LicelFile, save_netcdf
import click

class FileCreateHandler(FileSystemEventHandler):
    """
    """
    def __init__(self, outpath, filter, nc_output, netcdf):
        super(FileCreateHandler, self).__init__()
        self.outpath = outpath
        self.filter = filter
        self.netcdf = netcdf
        self.nc_output = nc_output
        
    def on_created(self, evt):
        """
        """
        f = LicelFile(evt.src_path, filter=self.filter)
        
        f.save(self.outpath)
        
        if self.netcdf:
            save_netcdf([f], self.nc_output)
        

class Application(object):
    
    def createWidgets(self):
        Label(self.root, text=u"Путь для сканирования:").grid(row=0,column=0,sticky='E')

        self.inpath = Entry(self.root)
        self.inpath.grid(row=0,column=1)

        Label(self.root, text=u"Фильтрованные данных:").grid(row=2,column=0,sticky='E')
        self.outpath=Entry(self.root)
        self.outpath.grid(row=2, column=1)

        Label(self.root, text=u"Файлы netCDF4:").grid(row=3, column=0,sticky='E')
        self.ncpath=Entry(self.root)
        self.ncpath.grid(row=3, column=1)

        Checkbutton(self.root, text=u"Фильтр?", variable=self.filter).grid(row=4,column=0, sticky='E')
    
        self.btnStartScan = Button(self.root, text=u"Следить", command=self.start_observe).grid(row=5,column=0)
        self.btnStopScan = Button(self.root, text=u"Остановить", command=self.stop_observe).grid(row=5,column=1)

        

    def start_observe(self):
        inpath = self.inpath.get()
        outpath= self.outpath.get()
        ncpath = self.ncpath.get()
        
        event_handler = FileCreateHandler(outpath, self.filter.get(), ncpath, False) 
        self.observer = Observer()
        self.observer.schedule(event_handler, inpath, recursive=False)
        self.observer.start()
        pass
    
    def stop_observe(self):
        self.observer.stop()
        self.observer.join()
        
    def on_closing(self):
        print("Closing")
        if hasattr(self, 'observer'):
            self.stop_observe()
        self.root.destroy()
        
    def __init__(self):
        root = self.root = Tk()
        root.title('File converter')
        
        self.filter = IntVar()
        self.createWidgets()
        
        root.protocol('WM_DELETE_WINDOW', self.on_closing)


def start():
    app = Application()
    app.root.mainloop()
    

if __name__=='__main__':
    start()
    