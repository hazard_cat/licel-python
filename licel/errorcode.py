from . import licel
import numpy as np
from scipy.integrate import cumtrapz
import sys

def calcerror(files):

    NFILES = len(files)
    NDATASETS=7
    idA355, idA532p, idA532s, idA1064, idP355, idP532p, idP532s  = range(NDATASETS)
    
    DATA = np.zeros((NDATASETS, NFILES, licel.MAX_PROFILE_LEN))
    
    NGLUED = 4
    
    DATAGL = np.zeros((NGLUED, NFILES, 500))
    


    print("Processing",end='')
    for ifile, fname in enumerate(files):
        print(".", end='')
        f=licel.LicelFile(fname, filter=False)
        
       
        
        #Zenith = f.zenith
        for item in f.dataset:
            
            #Range = np.arange(licel.MAX_PROFILE_LEN)*item.binWidth
            if item.wavelength==355:
                #print(item.wavelength, item.deviceId)
                
                if not item.isPhoton:
                    MAX_ADC = float(1<<item.adcBits)
                    #print(MAX_ADC, item.nShots, item.discrLevel)
                    DATA[idA355, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/(item.nShots*MAX_ADC)*item.discrLevel*1000
                else:
                    dt = (2*item.binWidth/licel.SPEED_OF_LIGHT)
                    DATA[idP355, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/item.nShots/dt
            
            elif item.wavelength==532 and item.polarization=='p':
                #print(item.wavelength, item.deviceId)
                
                if not item.isPhoton:
                    MAX_ADC = float(1<<item.adcBits)
                    #print(MAX_ADC, item.nShots, item.discrLevel)
                    DATA[idA532p, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/(item.nShots*MAX_ADC)*item.discrLevel*1000
                else:
                    dt = (2*item.binWidth/licel.SPEED_OF_LIGHT)
                    DATA[idP532p, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/item.nShots/dt
            elif item.wavelength==532 and item.polarization=='s':
                #print(item.wavelength, item.deviceId)
                
                if not item.isPhoton:
                    MAX_ADC = float(1<<item.adcBits)
                    #print(MAX_ADC, item.nShots, item.discrLevel)
                    DATA[idA532s, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/(item.nShots*MAX_ADC)*item.discrLevel*1000
                else:
                    dt = (2*item.binWidth/licel.SPEED_OF_LIGHT)
                    DATA[idP532p, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/item.nShots/dt
            elif item.wavelength==1064:
                #print(item.wavelength, item.deviceId)
                
                if not item.isPhoton:
                    MAX_ADC = float(1<<item.adcBits)
                    #print(MAX_ADC, item.nShots, item.discrLevel)
                    DATA[idA1064, ifile, :] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/(item.nShots*MAX_ADC)*item.discrLevel*1000

        tmpbgrA = np.mean(DATA[idA355, ifile, -1000:])
        tmpPA = DATA[idA355, ifile, :] - tmpbgrA
        tmpPA,_ = licel.decimate(tmpPA, 5)
        tmpbgrP = np.mean(DATA[idP355, ifile, -1000:])
        tmpPP = DATA[idA355, ifile, :] - tmpbgrP
        tmpPP,_ = licel.decimate(tmpPP, 5)
        DATAGL[idA355, ifile, :]  = licel.glue_profiles(tmpPA, tmpPP, 1.0, 10.0)

        tmpbgrA = np.mean(DATA[idA532p, ifile, -1000:])
        tmpPA = DATA[idA532p, ifile, :] - tmpbgrA
        tmpPA,_ = licel.decimate(tmpPA, 5)
        tmpbgrP = np.mean(DATA[idP532p, ifile, -1000:])
        tmpPP = DATA[idA532p, ifile, :] - tmpbgrP
        tmpPP,_ = licel.decimate(tmpPP, 5)
        DATAGL[idA532p, ifile, :]  = licel.glue_profiles(tmpPA, tmpPP, 1.0, 10.0)

        tmpbgrA = np.mean(DATA[idA532s, ifile, -1000:])
        tmpPA = DATA[idA532s, ifile, :] - tmpbgrA
        tmpPA,_ = licel.decimate(tmpPA, 5)
        tmpbgrP = np.mean(DATA[idP532s, ifile, -1000:])
        tmpPP = DATA[idA532s, ifile, :] - tmpbgrP
        tmpPP,_ = licel.decimate(tmpPP, 5)
        DATAGL[idA532s, ifile, :]  = licel.glue_profiles(tmpPA, tmpPP, 1.0, 10.0)


        tmpbgrA = np.mean(DATA[idA1064, ifile, -1000:])
        tmpPA = DATA[idA1064, ifile, :] - tmpbgrA
        tmpPA,_ = licel.decimate(tmpPA, 5)
        DATAGL[idA1064, ifile, :]  = tmpPA

    meanGL = np.mean(DATAGL, axis=1)
    stdGL = np.std(DATAGL, axis=1)


    epsilon = stdGL/meanGL
    print()
    H = np.arange(500)*240
    OUT = np.c_[H, epsilon.T]
    np.savetxt(sys.stdout, OUT)
