from . import licel
import numpy as np
from scipy.integrate import cumtrapz
import sys

TAU_353 = 0.58049
TAU_408 = 0.32157




def process(filelist, modelfile, meteofile, decim_idx, combined=False, Cconst=15, display=False):
    Z, P, Rho = np.loadtxt(modelfile, unpack=True)
    P=P*100
    
    NFILES = len(filelist)

    ANALOG353=np.zeros((NFILES, licel.MAX_PROFILE_LEN))
    PHOTON353=np.zeros((NFILES, licel.MAX_PROFILE_LEN))
    PHOTON408=np.zeros((NFILES, licel.MAX_PROFILE_LEN))
    print("Processing",end='')
    for ifile, fname in enumerate(filelist):
        print(".", end='')
        f=licel.LicelFile(fname, filter=False)
        
       
        
        Zenith = f.zenith
        for item in f.dataset:
            
            Range = np.arange(licel.MAX_PROFILE_LEN)*item.binWidth
            if item.wavelength==353:
                #print(item.wavelength, item.deviceId)
                
                if not item.isPhoton:
                    MAX_ADC = float(1<<item.adcBits)
                    #print(MAX_ADC, item.nShots, item.discrLevel)
                    ANALOG353[ifile,:] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/(item.nShots*MAX_ADC)*item.discrLevel*1000
                else:
                    dt = (2*item.binWidth/licel.SPEED_OF_LIGHT)
                    PHOTON353[ifile,:] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/item.nShots/dt

            if (item.wavelength==408) and item.isPhoton:
                dt = (2*item.binWidth/licel.SPEED_OF_LIGHT)
                PHOTON408[ifile,:] = item.data[:licel.MAX_PROFILE_LEN].astype('float')/item.nShots/dt
    print()   
    
    #print(PHOTON408.shape)
    meanPHOTON353 = np.mean(PHOTON353, axis=0)
    meanANALOG353 = np.mean(ANALOG353, axis=0)
    meanPHOTON408 = np.mean(PHOTON408, axis=0)

    meanPHOTON408 = meanPHOTON408 - np.mean(meanPHOTON408[-1000:])
    meanPHOTON353 = meanPHOTON353 - np.mean(meanPHOTON353[-1000:])
    meanANALOG353 = meanANALOG353 - np.mean(meanANALOG353[-1000:])
    

    
    YY,_ = licel.decimate(meanPHOTON353, decim_idx)
    XX,_ = licel.decimate(meanANALOG353, decim_idx)
    ZZ,W = licel.decimate(meanPHOTON408, decim_idx)
    H = np.arange(licel.MAX_PROFILE_LEN/W)*licel.MIN_BIN*W
    
    

    # Gluing profiles
    #Glued = glue_profiles(meanANALOG353, meanPHOTON353, 1.0, 10.0)
    Glued_d = licel.glue_profiles(XX, YY, 1.0, 10.0)


    Height = H*np.cos(np.deg2rad(Zenith))
    idx_signal=(Height>200)&(Height<6000.0)
    idx_rho = (Height>0)&(Height<30000.0)
    Trend = np.interp(Height[idx_rho], Z, Rho)
    Trend = Trend/np.trapz(Trend, Height[idx_rho])
    Trend = np.interp(Height[idx_signal], Height[idx_rho], Trend)
    dAlpha = (TAU_408-TAU_353)*Trend#/np.cos(np.deg2rad(Zenith))

    
    # Height = Range*np.cos(np.deg2rad(Zenith))
    # idx_signal=(Height>200)&(Height<6000.0)
    # idx_rho = (Height>0)&(Height<30000.0)
    # Trend = np.interp(Height[idx_rho], Z, Rho)
    # Trend = Trend/np.trapz(Trend, Height[idx_rho])
    # Trend = np.interp(Height[idx_signal], Height[idx_rho], Trend)
    # dAlpha = (TAU_408-TAU_353)*Trend#/np.cos(np.deg2rad(Zenith))

    # w = meanPHOTON408[idx_signal][1:]/Glued[idx_signal][1:]*np.exp(cumtrapz(dAlpha,  Range[idx_signal]))
    w = ZZ[idx_signal][1:]/Glued_d[idx_signal][1:]*np.exp(cumtrapz(dAlpha,  H[idx_signal]))*Cconst
    if meteofile:
        p, z, _, _, relh, mixr, _, _, _, _, _ = np.loadtxt(meteofile, unpack=True) 
        mixr = np.interp(Height[idx_signal][1:], z, mixr)
        
        data = np.c_[Height[idx_signal][1:], w, mixr, ZZ[idx_signal][1:], Glued_d[idx_signal][1:], np.exp(cumtrapz(dAlpha,  H[idx_signal]))]
        np.savetxt(sys.stdout, data, fmt='%+12.5e', header='HGHT MIXRL MIXRS, S408 MIXRZ, S353G TRANS')
    else:
        data = np.c_[Height[idx_signal][1:], w, ZZ[idx_signal][1:], Glued_d[idx_signal][1:], np.exp(cumtrapz(dAlpha,  H[idx_signal]))]
        np.savetxt(sys.stdout, data, fmt='%+12.5e', header='HGHT MIXRL S408 S353G TRANS')

    if display and meteofile:
        import matplotlib.pyplot as plt
        plt.plot(w, Height[idx_signal][1:], mixr, Height[idx_signal][1:])
        plt.xlabel('Mixing ratio, (g/kg)')
        plt.ylabel('Altitude, (m.)')
        plt.grid()
        plt.show()
    elif display and (not meteofile):
        import matplotlib.pyplot as plt
        plt.plot(w, Height[idx_signal][1:])
        plt.xlabel('Mixing ratio, (g/kg)')
        plt.ylabel('Altitude, (m.)')
        plt.grid()
        plt.show()
    
    
    #plt.plot(mixr, z)
    #plt.xlabel('MIXR, g/kg')
    #plt.ylabel('Altitude, m')
    #plt.grid(True)
    #plt.show()