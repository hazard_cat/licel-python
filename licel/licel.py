import time
import os
#import netCDF4 as cdf
from datetime import datetime
import numpy as np
from .median import medfilt
#from scipy.signal import medfilt

TIME_UNITS = 'days since 1984-02-08 00:00:00' 
SPEED_OF_LIGHT=3e2
MAX_PROFILE_LEN = 16000
MIN_BIN=7.5


def decimate(Y, w):
    if w<0 or w>5:
        return Y    
    
    W = 1<<w

    newlen = int(MAX_PROFILE_LEN/W)
    ret =np.zeros((newlen))

    for i,_ in enumerate(ret):
        ret[i] = np.sum(Y[i*W:(i+1)*W])
    return ret/W, W

def glue_profiles(A, P, vA=1, vB=10):
    idx=(P>vA)&(P<=vB)
    idx_left = P>vB
    RATIO = np.mean(A[idx]/P[idx])
    
    Glued = P*RATIO
    Glued[idx] = 0.5*(A[idx]+P[idx]*RATIO)
    Glued[idx_left] = A[idx_left]
    return Glued


class LicelProfile:
    """
    Описание одного измерения
    """
    def __init__(self, s):
        """
        Конструктор разбирает по элементам строку s
        """
        tmp = s.decode('ascii').split()
        
        self.isActive = bool(int(tmp[0]))
        self.isPhoton = bool(int(tmp[1]))
        self.laserType = int(tmp[2])
        self.nDataPoints = int(tmp[3])
        self.reserved1 = int(tmp[4])
        self.highVoltage = int(tmp[5])
        self.binWidth = float(tmp[6])
        tmp1 = tmp[7].split('.')
        self.wavelength = float(tmp1[0])
        self.polarization = tmp1[1]
        self.reserved2 = int(tmp[8])
        self.reserved3 = int(tmp[9])
        self.binShift = int(tmp[10])
        self.decBinShift = int(tmp[11])
        self.adcBits = int(tmp[12])
        self.nShots = int(tmp[13])
        self.discrLevel = float(tmp[14])
        self.deviceId = tmp[15][:2]
        self.nCrate = int(tmp[15][2])
        self.data = np.zeros(self.nDataPoints, dtype=np.int32)

class LicelFile:
    """
    Содержт описание класа одного файла с лидарными данными
    """
    
    def __init__(self, fname=None, filter=False):
        """
        Коонструктор, который помимо загрузки данных
        осуществляет медианную фильтрацию
        """
        self.measurementSite = None
        self.measurementStartTime = None
        self.measurementStopTime = None
        self.altitudeAboveSeaLevel = 0.0
        self.longitude = 0.0
        self.latitude = 0.0
        self.zenith = 0.0
        
        self.laser1NShots = 0
        self.laser1Freq = 0
        self.laser2NShots = 0
        self.laser2Freq = 0
        self.nDatasets = 0
        self.laser3NShots = 0
        self.laser3Freq = 0
        self.__dataset = []
        
        self.fileloaded = False
        
        #print("Loading %s..."%fname)
        
        try:    
            with open(fname, 'rb') as f:
                self.load(f)
            self.fileloaded = True
        except FileNotFoundError as e:
            print(e)
        
        if filter:
            self.filter()
            
        #self.save("1.1")
        
    def load(self, f):
        """ Загрузка файла.
            f - обЪект открытого файла
        """
        #read first line
        self.firstline = f.readline()
        #read second line
        tmp = f.readline().decode('ascii').split()
        
        #parse second line
        self.measurementSite = tmp[0]
        self.measurementStartTime = datetime.strptime(tmp[1]+" "+tmp[2],\
             "%d/%m/%Y %H:%M:%S")
        self.measurementStopTime = datetime.strptime(tmp[3]+" "+tmp[4],\
             "%d/%m/%Y %H:%M:%S")
        self.altitudeAboveSeaLevel = float(tmp[5])
        self.longitude = float(tmp[6])
        self.latitude = float(tmp[7])
        self.zenith = float(tmp[8])
        
        # read third line
        tmp = f.readline().decode('ascii').split()
        
        #parse third line
        self.laser1NShots = int(tmp[0])
        self.laser1Freq = int(tmp[1])
        self.laser2NShots = int(tmp[2])
        self.laser2Freq = int(tmp[3])
        self.nDatasets = int(tmp[4])
        self.laser3NShots = int(tmp[5])
        self.laser3Freq = int(tmp[6])
        
        # read channels properties
        for i in range(self.nDatasets):
            tmp = f.readline()
            self.dataset.append(LicelProfile(tmp))
            
        f.readline()
        
        for item in self.dataset:
            item.data = np.fromfile(f, dtype=np.int32, count=item.nDataPoints)
            f.readline()
        
    @property
    def dataset(self):
        return self.__dataset
    
    @dataset.deleter
    def dataset(self):
        del self.__dataset
    
    def filter(self):
        """ Фильтруем только аналоговые каналы """
        for item in self.dataset:
            if not item.isPhoton:
                item.data = medfilt(item.data, 3).astype(np.int32)
        pass
        
    def save(self, path):
        if(self.fileloaded):
            self.save_licel(path)

    def save_licel(self, path):
        """ сохраняем в исходный формат Licel"""
        tmp1 = "g%02d%1x%02d%02d.%02d%02d%02d"% \
            (self.measurementStartTime.year-2000, \
            self.measurementStartTime.month,\
            self.measurementStartTime.day, \
            self.measurementStartTime.hour, \
            self.measurementStartTime.minute, \
            self.measurementStartTime.second, \
            self.measurementStartTime.microsecond)
        fname = "%s/%s"%(path, tmp1)
        
        tmp2 = "%8s %19s %19s %04.0f %06.1f %06.1f %02.0f" % (\
            self.measurementSite,\
            datetime.strftime(self.measurementStartTime, '%d/%m/%Y %H:%M:%S'),\
            datetime.strftime(self.measurementStopTime, '%d/%m/%Y %H:%M:%S'),\
            self.altitudeAboveSeaLevel,\
            self.longitude,\
            self.latitude,\
            self.zenith)
        
        tmp3 = "%07d %04d %07d %04d %02d %07d %04d" % (\
            self.laser1NShots,\
            self.laser1Freq,\
            self.laser2NShots,\
            self.laser2Freq,\
            self.nDatasets,\
            self.laser3NShots,\
            self.laser3Freq)
        
        with open(fname, "wb") as f:
            f.write(b" %-77s\r\n"%tmp1.encode('ascii'))
            f.write(b" %-77s\r\n"%tmp2.encode('ascii'))
            f.write(b" %-77s\r\n"%tmp3.encode('ascii'))

            for item in self.__dataset:
                tmpA = "%01d %01d %01d %05d %d %04d %4.2f %05.0f.%c %d %d %02d %03d %02d %06d "%(\
                    int(item.isActive),\
                    int(item.isPhoton),\
                    item.laserType,\
                    item.nDataPoints,\
                    item.reserved1,\
                    item.highVoltage,\
                    item.binWidth,\
                    item.wavelength,\
                    item.polarization,\
                    item.reserved2,\
                    item.reserved3,\
                    item.binShift,\
                    item.decBinShift,\
                    item.adcBits,\
                    item.nShots)
                
                if item.deviceId[1]=='T':
                    tmpB = "%5.3f " % item.discrLevel
                elif item.deviceId[1]=='C':
                    tmpB = "%6.4f " % item.discrLevel
            
                tmpC = "%s%x"%(item.deviceId, item.nCrate)
                tmpD = tmpA+tmpB+tmpC
                f.write(b" %-77s\r\n"%tmpD.encode('ascii'))
            
            f.write(b"\r\n")
            
            for item in self.__dataset:
                item.data.tofile(f)
                f.write(b"\r\n")    
            
def save_netcdf(L, path='./'):
    """
    Сохранение результата в файл netCDF
    """
    
    tmp1 = "g%02d%1x%02d%02d%02d%02d-%02d%1x%02d%02d%02d%02d.nc4"% \
        (L[0].measurementStartTime.year-2000, \
        L[0].measurementStartTime.month,\
        L[0].measurementStartTime.day, \
        L[0].measurementStartTime.hour, \
        L[0].measurementStartTime.minute, \
        L[0].measurementStartTime.second, \
        L[-1].measurementStartTime.year-2000, \
        L[-1].measurementStartTime.month,\
        L[-1].measurementStartTime.day, \
        L[-1].measurementStartTime.hour, \
        L[-1].measurementStartTime.minute, \
        L[-1].measurementStartTime.second
        )
    fname = "%s/%s"%(path, tmp1)
    
    F = cdf.Dataset(fname, 'w', format='NETCDF4_CLASSIC')
    F.createDimension('time', size=None)
    F.createDimension('length', size=16380)
    
    F.createVariable('zenith', 'f4', ('time',), zlib=True, fill_value=-99.99)
    F.variables['zenith'].setncatts({'description':'Zenith angle of lidar orientation',\
                                    'units':'degrees'})
    
    F.createVariable('measurement_start_time', 'f8', ('time',), zlib=True, fill_value=-99.9)
    F.variables['measurement_start_time'].units = TIME_UNITS
    F.variables['measurement_start_time'].calendar = 'standard'

    F.createVariable('measurement_stop_time', 'f8', ('time',), zlib=True, fill_value=-99.9)
    F.variables['measurement_stop_time'].units = TIME_UNITS
    F.variables['measurement_stop_time'].calendar = 'standard'
    
    F.createVariable('altitude_above_sealevel', 'f4', ('time',), zlib=True, fill_value=-99.9)
    F.variables['altitude_above_sealevel'].units = 'meters'
    
    F.createVariable('latitude', 'f4', ('time',), zlib=True, fill_value=-99.9)
    F.variables['latitude'].units = 'degrees North'

    F.createVariable('longitude', 'f4', ('time',), zlib=True, fill_value=-99.9)
    F.variables['longitude'].units = 'degrees East'

    F.createVariable('laser1NShots', 'i2', ('time'), zlib=True, fill_value=-99.9)
    F.createVariable('laser1Freq', 'i2', ('time'), zlib=True, fill_value=-99.9)
    
    F.createDimension('num_dataset', size=L[0].nDatasets)
    
    F.createVariable('active', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('photon_counting', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('laser_type', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('n_data_points', 'i4', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('high_voltage', 'i4', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('bin_width', 'f4', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('wavelength', 'f4', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('polarization', 'S1', ('time', 'num_dataset'), zlib=True, fill_value='o')
    F.createVariable('bin_shift', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('dec_bin_shift', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('adc_bits', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('number_of_shots', 'i4', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('discr_level', 'f4', ('time', 'num_dataset'), zlib=True, fill_value=0)
    F.createVariable('device_id', 'S1', ('time', 'num_dataset'), zlib=True, fill_value='A')
    F.createVariable('crate_num', 'i1', ('time', 'num_dataset'), zlib=True, fill_value=0)
    
    F.createVariable('Data', 'i4', ('time', 'num_dataset', 'length'), zlib=True, fill_value=0)
    
    
    F.measurement_site = L[0].measurementSite
    #F.measurement_start_time = cdf.date2num(L.measurementStartTime, 'days since 1984-02-08 00:00:00')
    #F.measurement_stop_time = cdf.date2num(L.measurementStopTime, 'days since 1984-02-08 00:00:00')
    
    i=0
    for licel in L:
        F.variables['measurement_start_time'][i] = cdf.date2num(licel.measurementStartTime, 'days since 1984-02-08 00:00:00')
        F.variables['measurement_stop_time'][i] = cdf.date2num(licel.measurementStopTime, 'days since 1984-02-08 00:00:00')
        F.variables['altitude_above_sealevel'][i] = licel.altitudeAboveSeaLevel
        F.variables['longitude'][i] = licel.longitude
        F.variables['latitude'][i] = licel.latitude
        F.variables['zenith'][i] = licel.zenith
        F.variables['laser1Freq'][i] = licel.laser1Freq
        F.variables['laser1NShots'][i] = licel.laser1NShots
        
        j=0
        for ds in licel.dataset:
            F.variables['active'][i,j] = ds.isActive
            F.variables['photon_counting'][i,j] = ds.isPhoton
            F.variables['laser_type'][i,j] = ds.laserType
            F.variables['n_data_points'][i,j] = ds.nDataPoints
            F.variables['high_voltage'][i,j] = ds.highVoltage
            F.variables['bin_width'][i,j] = ds.binWidth
            F.variables['wavelength'][i,j] = ds.wavelength
            F.variables['polarization'][i,j] = ds.polarization
            F.variables['bin_shift'][i,j] = ds.binShift
            F.variables['dec_bin_shift'][i,j] = ds.decBinShift
            F.variables['adc_bits'][i,j] = ds.adcBits
            F.variables['number_of_shots'][i,j] = ds.nShots
            F.variables['discr_level'][i,j] = ds.discrLevel
            F.variables['device_id'][i,j] = ds.deviceId[1]
            F.variables['crate_num'][i,j] = ds.nCrate
            F.variables['Data'][i,j,:] = ds.data[:]
            j+=1
        i+=1
        
    
    F.sync()
    F.close()

if __name__=="__main__":
    import sys
    import pylab as plt
    f=LicelFile("../b1991821.512241", filter=False)
    
    print(f.measurementStartTime, f.longitude, f.latitude, f.zenith)
    print("Size of LicelFile object is {!r}".format(sys.getsizeof(f)))

    ANALOG353=None
    PHOTON353=None
    PHOTON408=None
    Zenith = f.zenith
    for item in f.dataset:
        
        Range = np.arange(item.nDataPoints)*item.binWidth
        if item.wavelength==353:
            print(item.wavelength, item.deviceId)
            
            if not item.isPhoton:
                MAX_ADC = float(1<<item.adcBits)
                print(MAX_ADC, item.nShots, item.discrLevel)
                ANALOG353 = item.data[:].astype('float')/(item.nShots*MAX_ADC)*item.discrLevel*1000
            else:
                dt = (2*item.binWidth/SPEED_OF_LIGHT)
                PHOTON353 = item.data[:].astype('float')/item.nShots/dt

        if (item.wavelength==408) and item.isPhoton:
            dt = (2*item.binWidth/SPEED_OF_LIGHT)
            PHOTON408 = item.data[:].astype('float')/item.nShots/dt
            
    

    # Glue profiles
    PHOTON353 = PHOTON353 - np.mean(PHOTON353[-1000:])
    ANALOG353 = ANALOG353 - np.mean(ANALOG353[-1000:])
    idx=(PHOTON353>1.0)&(PHOTON353<=10.0)
    idx_left = PHOTON353>10.0
    RATIO = np.mean(ANALOG353[idx]/PHOTON353[idx])
    
    Glued = PHOTON353*RATIO
    Glued[idx] = 0.5*(ANALOG353[idx]+PHOTON353[idx]*RATIO)
    Glued[idx_left] = ANALOG353[idx_left]
    Height = Range*np.cos(np.deg2rad(Zenith))
    idx_signal=Height<6000.0
    plt.semilogy(Height[idx_signal], Glued[idx_signal], Height[idx_signal], PHOTON408[idx_signal])
    plt.plot(Height[idx_signal], PHOTON408[idx_signal]/Glued[idx_signal])
    plt.grid(True)
    #f.save("./")
    #f.save_netcdf('w.nc')
    plt.show()